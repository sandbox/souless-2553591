
-- SUMMARY --

The Comment time module allows you stop commenting during certain periods
of the day.


-- REQUIREMENTS --

None.


-- INSTALLATION --

 Install as usual.


-- CONFIGURATION --

 Settings and configuration page can be found in:
  Administer > Settings > Comment Time


-- CUSTOMIZATION --


-- TROUBLESHOOTING --


-- FAQ --


-- CONTACT --

Current maintainers
 Jarkko Oksanen (jOksanen) - http://drupal.org/user/2352292
