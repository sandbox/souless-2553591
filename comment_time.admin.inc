<?php

/**
 * @file
 * Admin page callbacks for the comment_time module.
 */

/**
 * Form to config comment time settings.
 */
function comment_time_admin() {
  $form = array();

  $form['comment_time_start_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Disable comments from this time'),
    '#description' => t('Disable comments from. Both values need to be set. Use a value such as "0100" or "1030"'),
    '#default_value' => variable_get('comment_time_start_time', 0),
    '#required' => TRUE,
  );

  $form['comment_time_end_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Enable comments after this time'),
    '#description' => t('Enable comments from. Both values need to be set. Use a value such as "0100" or "1030"'),
    '#default_value' => variable_get('comment_time_end_time', 0),
    '#required' => TRUE,
  );

  $form['comment_time_use_disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use disabled message?'),
    '#description' => t('Enable comments from. Both values need to be set. Use a value such as "0100" or "1030"'),
    '#default_value' => variable_get('comment_time_use_disabled', 0),
  );

  $form['comment_time_disabled_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Enable comments after this time'),
    '#description' => t('Enable comments from. Both values need to be set. Use a value such as "0100" or "1030"'),
    '#default_value' => variable_get('comment_time_disabled_message', "Commenting is disabled at this time."),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}


/**
 * Submit handler for comment_time admin form.
 *
 * Saves comment_times variable.
 */
function comment_time_admin_submit($form, &$form_state) {
  variable_set('comment_time_start_time', $form_state['values']['comment_time_start_time']);
  variable_set('comment_time_end_time', $form_state['values']['comment_time_end_time']);
  variable_set('comment_time_use_disabled', $form_state['values']['comment_time_use_disabled']);
  variable_set('comment_time_disabled_message', $form_state['values']['comment_time_disabled_message']);
  drupal_set_message(t('The configuration options have been saved.'));
}
